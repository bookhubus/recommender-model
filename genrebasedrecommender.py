# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 22:11:04 2020

@author: StarkTheGnr
"""

from settings import connection_string, test_env
import pandas as pd
import sys
from sqlalchemy import create_engine
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

def get_genre_recommendation(arg_book_id=0, arg_count=5, arg_pycall=False):
    if test_env or arg_pycall:
        book_id = arg_book_id
        count = arg_count
    else:
        book_id = sys.argv[1]
        count = sys.argv[2]
        if count is None or book_id is None:
            return
        
    conn = create_engine(connection_string)
        
    dataframe = pd.read_sql_query("SELECT id, title, genre, tag, author FROM book", conn)
    dataframe = dataframe.apply(parse_data_values, axis=1)
    dataframe = dataframe.apply(format_values, axis=1)
    dataframe = dataframe.apply(create_meta_soup, axis=1)
    
    count_vectorizer = CountVectorizer()
    count_matrix = count_vectorizer.fit_transform(dataframe['soup'])
    
    index = dataframe.index[dataframe['id'] == book_id]
    
    similarity_matrix = cosine_similarity(count_matrix[index], count_matrix)
    dataframe['similarity'] = similarity_matrix[0]
    
    dataframe = dataframe.sort_values('similarity', ascending=False)
    
    return dataframe['similarity'].head(count + 1)[1:]
    
    
def parse_data_values(datarow):
    datarow['genre'] = datarow['genre'].split(',')
    datarow['tag'] = datarow['tag'].split(',')
    datarow['author'] = datarow['author'].split(',')
    return datarow


def format_values(data):
    for i in range(len(data['genre'])):
        data['genre'][i] = data['genre'][i].lower()
        data['genre'][i] = data['genre'][i].replace(' ', '')
    
    for i in range(len(data['tag'])):
        data['tag'][i] = data['tag'][i].lower()
        data['tag'][i] = data['tag'][i].replace(' ', '')
        
    for i in range(len(data['author'])):
        data['author'][i] = data['author'][i].lower()
        data['author'][i] = data['author'][i].replace(' ', '')
        
    return data

def create_meta_soup(data):
    data['soup'] = " ".join(data['genre']) + " " + " ".join(data['genre'])
    data['soup'] += " " + " ".join(data['tag'])
    data['soup'] += " " + " ".join(data['author']) + " " + " ".join(data['author']) + " " + " ".join(data['author']) + " " + " ".join(data['author'])
    
    return data

if __name__ == "__main__":
    print(get_genre_recommendation())