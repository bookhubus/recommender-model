# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 00:23:02 2020

@author: StarkTheGnr
"""

from settings import test_env
from genrebasedrecommender import get_genre_recommendation
from contentbasedrecommender import get_recommendation
import sys

if test_env:
    book_id = 0
    count = 5
else:
    book_id = sys.argv[1]
    count = sys.argv[2]

genre_based_similarity = get_genre_recommendation(book_id, count, True)
content_based_similarity = get_recommendation(book_id, count, list(genre_based_similarity.keys()), True)

content_keys = list(content_based_similarity.keys())

total_similarity = []
total_similarity_ids = []
for i in range(count):
    genre_index = genre_based_similarity.keys()[i]
    
    if genre_index in content_keys:
        content_index = content_keys.index(genre_index)
        total_similarity.append(genre_based_similarity.values[i] + content_based_similarity.values[content_index])
    else:
        total_similarity.append(genre_based_similarity.values[i])
        
    total_similarity_ids.append(genre_index)
    
sorted_ids = [x for y, x in sorted(zip(total_similarity, total_similarity_ids), reverse=True)]

print(sorted_ids)