# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 18:30:14 2020

@author: StarkTheGnr
"""

from settings import connection_string, test_env
import pandas as pd
import sys
from sqlalchemy import create_engine
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

def get_recommendation(arg_book_id=0, arg_count=5, arg_important_books=[], arg_pycall=False):
    if test_env or arg_pycall:
        book_id = arg_book_id
        count = arg_count
    else:
        count = sys.argv[1]
        book_id = sys.argv[2]
        if count is None or book_id is None:
            return
    
    conn = create_engine(connection_string)
    
    dataframe = pd.read_sql_query("SELECT id, title, description FROM book", conn)
    dataframe['description'] = dataframe['description'].fillna('')
    
    movieIndex = dataframe.index[dataframe['id'] == book_id][0]
    
    tfidf = TfidfVectorizer(stop_words="english")
    word_matrix = tfidf.fit_transform(dataframe['description'])
    
    similarity_matrix = linear_kernel(word_matrix[movieIndex], word_matrix)
    
    dataframe['similarity'] = similarity_matrix[0]
    
    if len(arg_important_books) is not 0:
        dataframe = dataframe.query("id in @arg_important_books")
        return dataframe['similarity']
    
    dataframe = dataframe.sort_values('similarity', ascending=False)
    
    return dataframe['similarity'].head(count + 1)[1:]

if __name__ == "__main__":
    print(get_recommendation())

