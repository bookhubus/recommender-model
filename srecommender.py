# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 16:17:52 2020

@author: StarkTheGnr
"""

from settings import connection_string, test_env, default_min_request
import pandas as pd
from sqlalchemy import create_engine

def get_recommendation():
    if test_env:
        min_req = 120
    else:
        min_req = sys.argv[1]
        if min_req is None:
            min_req = default_min_request

    conn = create_engine(connection_string)
    
    dataframe = pd.read_sql_query("SELECT title, rating, voteCount FROM book", conn)
    
    mean = dataframe['rating'].mean()
    
    def weighted_rating(dr, minimum_ratings=min_req, average_votes=mean):
        votes = dr['voteCount']
        rating = dr['rating']
        
        result = (votes/(votes + minimum_ratings) * rating) + (minimum_ratings/(minimum_ratings + votes) * average_votes)
        return result
    
    dataframe['score'] = dataframe.apply(weighted_rating, axis=1)
    dataframe = dataframe.sort_values('score', ascending=False)
    
    print(dataframe.head(20))
    return

get_recommendation()